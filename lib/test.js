import React from 'react';
import {FramedComponent, Controller} from './index';
import spec from './test-spec';
import System from 'tvs-entity-system/lib/runtime';
import SpecManager from 'tvs-entity-system/lib/spec-manager';
import Parser from 'tvs-entity-system/lib/parsers/object-spec';

var sys = System.create();
var basespec = Parser.parse(spec);
SpecManager.loadSpec(sys, basespec);

var controller = Controller.create(basespec, sys);

React.render(<FramedComponent controller={controller} />, document.getElementById('root'));

window.sys = sys;
window.controller = controller;
