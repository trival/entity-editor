import React, { Component } from 'react';
import Draggable from 'react-draggable';
import EntitySystem from './main';
require('../style/frame.styl');

export default class EntitySystemFrame extends Component {

    constructor(props) {
        super(props);
        this.state = {
            style: {
                width: props.width || window.innerWidth * 0.9,
                height: props.height || window.innerHeight * 0.9,
                top: props.top || window.innerHeight * 0.05,
                left: props.left || window.innerWidth * 0.05
            }
        };
    }

    render() {
        return (
            <Draggable
                handle=".tsee-drag-handle"
                zIndex={100}>

                <div className="tsee-frame" style={this.state.style}>

                    <EntitySystem
                        dragHandleClassName="tsee-drag-handle"
                        controller={this.props.controller} />

                    <Draggable
                        onStart={this.onResizeStart.bind(this)}
                        onDrag={this.onResizeDrag.bind(this)}
                        onStop={this.onRsizeEnd.bind(this)}
                        ref="resizeHandle">
                        <span className="tsee-resize-handle"></span>
                    </Draggable>

                </div>

            </Draggable>
        );
    }

    onResizeStart() {
        this.resizeStartWidth = this.state.style.width;
        this.resizeStartHeight = this.state.style.height;
    }

    onResizeDrag(e, {position: {left, top}}) {
        this.setState({
            style: {
                width: this.resizeStartWidth + left,
                height: this.resizeStartHeight + top
            }
        });
    }

    onRsizeEnd() {
        this.refs.resizeHandle.resetState();
    }
}

