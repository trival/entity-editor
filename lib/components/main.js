import React, { Component } from 'react';
import EntityGraph from './entity-graph';
import EntityDetails from './entity-details';
import Immutable from 'immutable';
require('../style/main.styl');

export default class EntitySystemEditor extends Component {

  constructor(props) {
    super(props);
    this.state = {value: props.controller.getState()};
  }

  componentDidMount() {
    this._onControllerChange = this.onControllerChange.bind(this);
    this.props.controller.onChange(this._onControllerChange);
  }

  componentWillUnmount() {
    this.props.controller.offChange(this._onControllerChange);
  }

  onControllerChange() {
    this.setState({value: this.props.controller.getState()});
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !Immutable.is(nextState.value, this.state.value);
  }

  render() {
    let spec = this.state.value.get('spec'),
      entities = spec.entities,
      openEntities = this.state.value.get('openEntities'),
      editedEntityValues = this.state.value.get('editedEntityValues'),
      controller = this.props.controller,
      dragHandle = this.props.dragHandleClassName;

    return (
      <article className="tsee-main">
        <header className={dragHandle}>
          <h3 className={dragHandle}>System Editor</h3>
        </header>

        <EntityGraph spec={spec} controller={controller}/>

        <section className="tsee-open-entities">
          <ul>{
            openEntities.map((id, index) => {
              let entity = entities[id];
              return <li>
                <EntityDetails
                  controller={controller}
                  entity={entity}
                  editingValue={editedEntityValues.contains(id)}
                  index={index}/>
              </li>;
            })
          }</ul>
        </section>
      </article>
    );
  }
}

