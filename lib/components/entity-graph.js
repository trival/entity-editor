import React, {
  Component
}
from 'react';
import Network from 'vis/lib/network/Network';

const DEFAULT_GROUP = '__default';

export default class EntityGraph extends Component {

  componentDidMount() {
    var options = {
      layout: {
        randomSeed: 3
          // hierarchical: {
          // enabled: true,
          // // sortMethod: 'directed'
          // }
      },
      edges: {
        arrows: 'to',
        smooth: false,
        color: {
          inherit: 'to'
        }
      },
      nodes: {
        shape: 'box'
      },
      groups: {
        [DEFAULT_GROUP]: {
          color: {
            background: '#ddd',
            border: '#aaa',
            highlight: {
              background: '#fff',
              border: '#ddd'
            }
          }
        }
      },
      physics: {
        enabled: true,
        forceAtlas2Based: {
          avoidOverlap: 0.4,
          gravitationalConstant: -70,
          springConstant: 0.05
        },
        barnesHut: {
          avoidOverlap: 0.2,
          gravitationalConstant: -3000
        },
        solver: 'forceAtlas2Based',
        stabilization: {
          iterations: 2000
        }
      }
    };

    this.graph = new Network(React.findDOMNode(this));
    this.graph.setOptions(options);
    this.graph.on('selectNode', this.onNodeSelected.bind(this));
    this.renderGraph();
  }

  onNodeSelected(event) {
    this.props.controller.openEntity(event.nodes[0]);
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.spec !== this.props.spec;
  }

  componentDidUpdate() {
    this.renderGraph();
  }

  componentWillUnmount() {
    this.graph.destroy();
  }

  render() {
    return <section id = {
      this.props.id
    }
    className = "tsee-entity-graph" > < /section>;
  }

  renderGraph() {

    var nodes = [],
      edges = [];

    for (let k in this.props.spec.entities) {
      let e = this.props.spec.entities[k],
        label = e.name,
        ns = e.namespace;
      if (ns) {
        label += '\n' + ns;
      }
      nodes.push({
        id: e.id,
        group: ns || DEFAULT_GROUP,
        label
      });
    }

    for (let k in this.props.spec.factories) {
      let f = this.props.spec.factories[k],
        deps = f.dependencies;
      if (deps && deps.length) {
        let to = f.receiver;
        deps.forEach(d => {
          edges.push({
            from: d,
            to
          });
        });
      }
    }

    for (let k in this.props.spec.oscillators) {
      let o = this.props.spec.oscillators[k],
        deps = o.dependencies;

      edges.push({
        from: o.receiver,
        to: o.target,
        dashes: true
      });

      if (deps && deps.length) {
        let to = o.receiver;
        deps.forEach(d => {
          edges.push({
            from: d,
            to
          });
        });
      }
    }

    for (let k in this.props.spec.reactions) {
      let r = this.props.spec.reactions[k],
        to = r.receiver;
      r.triggers.forEach(t => {
        edges.push({
          from: t,
          to
        });
      });
    }

    var graph = {
      nodes,
      edges
    };

    this.graph.setData(graph);
  }
}
