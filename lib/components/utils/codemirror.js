import React, { Component } from 'react'
import CM from 'codemirror'

require('./glsl')
require('codemirror/mode/coffeescript/coffeescript')
require('codemirror/mode/javascript/javascript')
require('codemirror/mode/htmlmixed/htmlmixed')
require('codemirror/keymap/vim')
require('codemirror/addon/edit/closebrackets')
require('codemirror/addon/edit/closetag')
require('codemirror/addon/edit/matchbrackets')
require('codemirror/addon/edit/matchtags')
// require('codemirror/addon/hint/')
require('codemirror/theme/tomorrow-night-bright.css')
require('codemirror/lib/codemirror.css')
require('./codemirror.css')


const defaultCMOptions = {
  keyMap: 'vim',
  lineNumbers: true,
  matchBrackets: true,
  showCursorWhenSelecting: true,
  theme: 'tomorrow-night-bright',
  viewportMargin: Infinity,
  scrollbarStyle: "null"
}


export default class CodeMirror extends Component {

  componentDidMount() {
    this.codeMirror = CM(this.refs.codemirror.getDOMNode(), {
      ...defaultCMOptions,
      ...this.props.options
    })
    this.codeMirror.setValue(this.props.value || "")
    CM.Vim.map("jj", "<Esc>", "insert")
    CM.Vim.map("kk", "<Esc>", "insert")
    this._currentCodemirrorValue = this.props.value
  }

  componentWillUnmount() {
    if (this.codeMirror) {
      let myNode = this.refs.codemirror.getDOMNode()

      while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild)
      }

      this.codeMirror = null
    }
  }

  componentWillReceiveProps(nextProps) {
    this.codeMirror.setValue(nextProps.value)

    for (let option in nextProps.options) {
      let optValue = nextProps.options[option]
      if (this.codeMirror.getOption(option) !== optValue) {
        this.codeMirror.setOption(option, optValue)
      }
    }
  }

  getValue() {
    return this.codeMirror.getValue()
  }

  render() {
    return (
      <div ref='codemirror' className="codemirror-container" />
    )
  }
}
