import React, { Component } from 'react'
import CodeMirror from '../utils/codemirror'
require('./JSON.styl')

export default class JSONValue extends Component {

  static defaultProps = {
    editingValue: false
  }

  static shouldComponentUpdate() {
    return true
  }

  render() {
    let editingValue = this.props.editingValue,
        valueJSON = JSON.stringify(this.props.value, null, '    '),
        interactions, representation

    if (editingValue) {
      let cmOptions = {
        mode: 'javascript'
      }

      representation = (
        <CodeMirror ref="editor" value={valueJSON} options={cmOptions}/>
      )

      let update = () => {
        let newValue = JSON.parse(this.refs.editor.getValue())
        this.props.updateValue(newValue)
        this.props.finishEdit()
      }

      let cancel = () => {
        this.props.finishEdit()
      }

      interactions = (
        <span>
          <button onClick={update}>update</button>
          <button onClick={cancel}>cancel</button>
        </span>
      )

    } else {
      representation = (
        <code>
          <pre>{valueJSON}</pre>
        </code>
      )

      interactions = (
        <span>
          <button onClick={this.props.startEdit.bind(this)}>edit</button>
        </span>
      )
    }

    return (
      <section className="entity-value-json">
        <h4>
          Value:
          {interactions}
        </h4>
        {representation}
      </section>
    )
  }
}

