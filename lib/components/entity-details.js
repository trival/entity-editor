import React, { Component } from 'react'
import JSONValue from './valueTypes/JSON'

export default class EntityDetails extends Component {

  static defaultProps = {
    editingValue: false
  }

  constructor(props) {
    super(props)
    this.state = {
      value: props.controller.getValue(props.entity.id)
    }
  }

  componentDidMount() {
    if (!this.props.editingValue) {
      this.addCallback(this.props.entity.id)
    }
  }

  componentWillUnmount() {
    this.removeCallback()
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (nextProps.entity !== this.props.entity) ||
      (nextProps.editingValue !== this.props.editingValue)
  }

  componentDidUpdate(prevProps) {
    let oldId = prevProps.entity.id,
        id = this.props.entity.id,
        oldEditingValue = prevProps.editingValue,
        editingValue = this.props.editingValue,
        idChanged = id !== oldId,
        editingValueChanged = oldEditingValue !== editingValue

    if ((idChanged && !editingValue) ||
      (editingValueChanged && editingValue)) {
      this.removeCallback()
    }

    if ((idChanged && !editingValue) ||
      (editingValueChanged && !editingValue)) {
      this.addCallback(id)
    } else if (idChanged) {
      this.setState({value: this.props.controller.getValue(id)})
      this.forceUpdate()
    }
  }

  addCallback(id) {
    this.callback = this.props.controller.setCallback(id, function (value) {
      this.setState({value})
      this.forceUpdate()
    }.bind(this))
  }

  removeCallback() {
    if (this.callback) {
      this.props.controller.removeCallback(this.callback.id)
      this.callback = null
    }
  }

  render() {
    let title = this.props.entity.name,
      id = this.props.entity.id,
      value = this.state.value,
      namespace = this.props.entity.namespace,
      editingValue = this.props.editingValue,
      updateValue = (value) => {
        this.props.controller.setEntity(id, value)
      },
      startEdit = () => {
        controller.editEntityValue(id)
      },
      finishEdit = () => {
        controller.watchEntityValue(id)
      },
      valueRepresentation = (
        <JSONValue
          value={value}
          editingValue={editingValue}
          updateValue={updateValue}
          startEdit={startEdit}
          finishEdit={finishEdit}/>
      )


    if (namespace) {
      title += ' / ' + namespace
    }

    return (
      <article className="tsee-entity-details">
        <header>
          <h3>
            <a className="tsee-close-button" onClick={this.onCloseClick.bind(this)} href="#">x</a>
            {title}
          </h3>
        </header>
        {valueRepresentation}
      </article>
    )
  }

  onCloseClick(e) {
    e.preventDefault()
    e.stopPropagation()
    this.props.controller.closeEntity(this.props.index)
  }

}
