# ES = require 'tvs-entity-system/lib/runtime'
# SM = require 'tvs-entity-system/lib/spec-manager'
Immutable = require 'immutable'
{Set, List, Map} = Immutable



# ===== Controller initializer =====

module.exports.create = (manager, system) ->

  callbacks = []
  openEntities = List()
  editedEntityValues = Set()


  # ===== Setup and change managment =====

  getState = ->
    Map {
      spec: manager.getSpec()
      openEntities
      editedEntityValues
    }


  onChange = (callback) ->
    callbacks.push callback
    return


  offChange = (callback) ->
    callbacks = callbacks.filter (c) -> c isnt callback


  triggerChange = ->
    callback() for callback in callbacks
    return


  # ===== System manipulation =====

  setEntity = (eid, value) ->
    system.set eid, value
    system.flush()
    return


  touchEntity = (eid) ->
    system.touch eid
    system.flush()
    return


  getValue = (eid) ->
    system.get eid


  setCallback = (eid, cb) ->
    callback = system.addCallback
      procedure: cb
      triggers: [eid]
    requestAnimationFrame -> system.callAction callback.id
    callback


  removeCallback = (cid) ->
    system.removeCallback cid


  # ===== Entity editor state =====

  openEntity = (id) ->
    if manager.getEntityById(id) and not openEntities.includes id
      openEntities = openEntities.push id
      triggerChange()
    return


  closeEntity = (index) ->
    openEntities = openEntities.remove index
    triggerChange()
    return


  editEntityValue = (id) ->
    if manager.getEntityById id
      editedEntityValues = editedEntityValues.add id
      triggerChange()
    return


  watchEntityValue = (id) ->
    editedEntityValues = editedEntityValues.remove id
    triggerChange()
    return


  # ===== interface =====

  return {
    getState
    onChange
    offChange
    triggerChange

    setEntity
    touchEntity
    getValue
    setCallback
    removeCallback

    openEntity
    closeEntity
    editEntityValue
    watchEntityValue
  }
