module.exports = {
    Controller: require('./controller/main'),
    Component: require('./components/main'),
    FramedComponent: require('./components/mainframe')
};
