inc = (x) -> x + 1
length = (x) -> x.length

module.exports =

  'counter':
    value: 0
    reactions:
      'trigger': inc

  'counterAcc':
    value: []
    reactions:
      'counter': (acc, cnt) ->
        acc.push cnt
        return

  'accLength':
    init:
      require: 'counterAcc'
      do: length


