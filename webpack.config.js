var path = require('path');
var webpack = require('webpack');

module.exports = {
    devtool: '#cheap-module-eval-source-map',

    entry: [
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './lib/test'
    ],

    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],

    resolve: {
        extensions: ['', '.js', '.jsx', '.coffee'],
        root: [
            path.resolve(__dirname, './lib'),
            path.resolve(__dirname, './custom_modules')
        ],
    },

    module: {
        loaders: [{
            test: /\.jsx?$/,
            loaders: ['react-hot', 'babel'],
            include: [
                path.join(__dirname, 'lib'),
                path.join(__dirname, 'node_modules', 'vis')
            ]
            // exclude: /node_modules/
        }, {
            loader: 'coffee',
            test: /\.coffee$/
        }, {
            loader: 'style!raw!stylus',
            test: /\.styl$/
        }, {
            loader: 'style!raw',
            test: /\.css$/
        }]
    }
};
